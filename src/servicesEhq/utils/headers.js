import localforage from 'localforage'

export class Header {
  static async getHeaders() {
    const accessKey = await localforage.getItem('accessKeyEHQ')
    const authenticationKey = await localforage.getItem('authenticationKeyEHQ')
    return {
      'access-key': accessKey,
      'authentication-key': authenticationKey,
    }
  }
}
