import http from '../utils/http'
import { Header } from '../utils/headers'

export default {
  async getClasses(studentId) {
    let ret = await http.get(`api/student/classes/${studentId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  // data :{classId,studentId}
  async enterTest(data) {
    let ret = await http.post(
      `api/student/test/enter`,
      { classId: data.classId, studentId: data.studentId },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  async runTest(studentId, testId) {
    let ret = await http.get(`api/student/getTest/${studentId}/${testId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  // data: {questionsId,studentId,testId,answers}
  async answerTestQuestions(data) {
    let ret = await http.post(`api/student/question/answer`, data, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },
}
