import http from '../utils/http'
import { Header } from '../utils/headers'

export default {
  //método executado por admin

  async addMonitor(user) {
    let ret = await http.post(
      'api/admin/teacher/addMonitor',
      {
        teacherId: user.teacherId,
        monitorId: user.monitorId,
      },
      { headers: await Header.getHeaders() }
    )
    return ret.data
  },
  async moveMonitor(monitor) {
    let ret = await http.post(
      'api/admin/teacher/moveMonitor',
      {
        currentTeacherId: monitor.currentTeacherId,
        newTeacherId: monitor.newTeacherId,
        monitorId: monitor.monitorId,
      },
      { headers: await Header.getHeaders() }
    )
    return ret.data
  },
  async deleteTeacher(teacherId) {
    let ret = await http.delete(`api/admin/teacher/delete/${teacherId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  // métodos executados por professores e por admins

  async readPublicQuestions() {
    let ret = await http.get(`api/teacher/question/read`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  async readQuestionsByArea(areaId) {
    let ret = await http.get(`api/teacher/question/read/area/${areaId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  async readQuestionsByTeacher(id) {
    let ret = await http.get(`api/teacher/question/read/own/${id}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  async getAllAvailableAreas() {
    let ret = await http.get(`api/teacher/area/readAll`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  async newQuestion(data) {
    let ret = await http.post(
      `api/teacher/question/create`,
      {
        title: data.title,
        body: data.body,
        questionType: data.questionType,
        visibility: data.visibility,
        difficulty: data.difficulty,
        correctAnswers: data.correctAnswers,
        incorrectAnswers: data.incorrectAnswers,
        author: data.author,
        area: data.area,
      },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  async deleteQuestion(teacherId, questionId) {
    let ret = await http.post(
      `api/teacher/question/remove`,
      {
        teacherId: teacherId,
        questionId: questionId,
      },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  // editando-> removendo resposta da pergunta -> answers = []
  async editRemoveAnswer(teacherId, questionId, answers) {
    let ret = await http.post(
      `api/teacher/question/remove/answers`,
      {
        teacherId: teacherId,
        questionId: questionId,
        answers: answers,
      },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  //answers é obj que contem correctAnswers e incorrectAnswers
  async editAddAnswer(teacherId, questionId, answers) {
    let ret = await http.post(
      `api/teacher/question/add/answers`,
      {
        teacherId: teacherId,
        questionId: questionId,
        answers: answers,
      },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  async editRemoveArea(teacherId, questionId, areas) {
    let ret = await http.post(
      `api/teacher/question/remove/area`,
      {
        teacherId: teacherId,
        questionId: questionId,
        areas: areas,
      },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  async editAddArea(teacherId, questionId, areas) {
    let ret = await http.post(
      `api/teacher/question/add/area`,
      {
        teacherId: teacherId,
        questionId: questionId,
        areas: areas,
      },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  // para editar outros dados como: titulo, dificuldade, tipo, visibilidade, enunciado
  async editQuestionData(teacherId, questionId, update) {
    let ret = await http.post(
      `api/teacher/question/update`,
      {
        teacherId: teacherId,
        questionId: questionId,
        update: update,
      },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  // PROVAS

  async readAllTests(teacherId) {
    let ret = await http.get(`api/teacher/test/readAll/${teacherId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  async readAllAppliedTests(testId) {
    let ret = await http.get(`api/teacher/testApplication/read/${testId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  async readTestById(testId) {
    let ret = await http.get(`api/teacher/test/${testId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  async readMonitorByTeacher(teacherId) {
    let ret = await http.get(`api/teacher/monitor/read/${teacherId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  async readClassesByTeacher(teacherId) {
    let ret = await http.get(`api/teacher/class/read/${teacherId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  //data {teacherId,instructions,initialDate,endDate,title,class,questions,monitors}
  async createTest(data) {
    let ret = await http.post(
      `api/teacher/test/create`,
      {
        teacherId: data.teacherId,
        instructions: data.instructions,
        initialDate: data.initialDate,
        endDate: data.endDate,
        title: data.title,
        class: data.class,
        questions: data.questions,
        monitors: data.monitors,
      },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  async updateTest(id, initialDate, endDate, title, instructions) {
    let ret = await http.post(
      `api/teacher/test/update`,
      {
        id: id,
        update: { initialDate, endDate, title, instructions },
      },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  async deleteTest(id) {
    let ret = await http.delete(`api/teacher/test/${id}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },
}
