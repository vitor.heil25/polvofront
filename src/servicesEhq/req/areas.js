import http from "../utils/http";
import { Header } from "../utils/headers";

export default {
  async createArea(area) {
    let ret = await http.post(
      "api/admin/area/create",
      {
        name: area.name,
      },
      { headers: await Header.getHeaders() }
    );
    return ret.data;
  },
  async createSubArea(subArea) {
    let ret = await http.post(
      "api/admin/area/children/create",
      {
        name: subArea.name,
        daddy: subArea.daddy,
      },
      { headers: await Header.getHeaders() }
    );
    return ret.data;
  },
  async moveSubArea(data) {
    let ret = await http.post(
      "api/admin/area/move",
      {
        areaId: data.areaId,
        from: data.from,
        to: data.to,
      },
      { headers: await Header.getHeaders() }
    );
    return ret.data;
  },
  async readChildrenArea(areaId) {
    let ret = await http.get(
      `api/admin/area/children/read/${areaId}`,
      { headers: await Header.getHeaders() }
    );
    return ret.data;
  },
  async readRootAreas() {
    let ret = await http.get(
      "api/admin/area/read",
      { headers: await Header.getHeaders() }
    );
    return ret.data;
  },
  async updateArea(areaId, update) {
    let ret = await http.post(
      "api/admin/area/update",
      {
        areaId: areaId,
        update: {
          name: update.name,
        },
      },
      { headers: await Header.getHeaders() }
    );
    return ret.data;
  },
  async deleteArea(areaId) {
    let ret = await http.delete(`api/admin/area/delete/${areaId}`,
      { headers: await Header.getHeaders() }
    );
    return ret.data;
  },
};
