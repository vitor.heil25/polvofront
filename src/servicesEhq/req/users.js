import http from '../utils/http'
import { Header } from '../utils/headers'

export default {
  async createUser(user) {
    let ret = await http.post(
      'api/admin/user/create',
      {
        name: user.name,
        surname: user.surname,
        email: user.email,
        password: user.password,
        type: user.type,
      },
      { headers: await Header.getHeaders() }
    )
    return ret.data
  },
  async createUserMonitor(userMonitor) {
    let ret = await http.post(
      'api/admin/user/monitor/create',
      {
        name: userMonitor.name,
        surname: userMonitor.surname,
        email: userMonitor.email,
        password: userMonitor.password,
        teacherId: userMonitor.teacherId,
      },
      { headers: await Header.getHeaders() }
    )
    return ret.data
  },
  async classUpload(classe, students) {
    let ret = await http.post(
      'api/admin/user/class/create',
      {
        name: classe.name,
        teacherId: classe.teacherId,
        students: {
          email: students.email,
          name: students.name,
          surname: students.surname,
          password: students.password,
        },
      },
      { headers: await Header.getHeaders() }
    )
    return ret.data
  },
  async updateUserInfos(userId, update) {
    let ret = await http.post(
      'api/admin/user/update',
      {
        userId: userId,
        update: {
          name: update.name,
          surname: update.surname,
          email: update.email,
        },
      },
      { headers: await Header.getHeaders() }
    )
    return ret.data
  },
  async readUserbyId(userId) {
    let ret = await http.get(`api/admin/user/read/${userId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },
  async readUserbyType(userType) {
    let ret = await http.get(`api/admin/user/readByType/${userType}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },
}
