import http from "../utils/http";

export default {
  async login(user) {
    let ret = await http.post("/api/login", {
      login: user.login,
      password: user.password,
    });
    return ret.data;
  },
};
