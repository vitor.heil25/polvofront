import http from '../utils/http'
import { Header } from '../utils/headers'

export default {
  // para admin
  async deleteMonitor(monitorId) {
    let ret = await http.delete(`api/admin/monitor/delete/${monitorId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  async readMonitorByTeacherId(teacherId) {
    let ret = await http.get(`api/admin/monitor/read/${teacherId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },

  // métodos para monitor

  async enterTestMonitoring(monitorId) {
    let ret = await http.post(
      `api/monitor/test/enter`,
      { monitorId },
      {
        headers: await Header.getHeaders(),
      }
    )
    return ret.data
  },

  async readTests(monitorId) {
    let ret = await http.get(`api/monitor/test/read/${monitorId}`, {
      headers: await Header.getHeaders(),
    })
    return ret.data
  },
}
