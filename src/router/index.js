import Vue from 'vue'
import VueRouter from 'vue-router'
import Roles from '../views/management/Roles.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue'),
  },
  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/drive',
    naem: 'Drive',
    component: () => import('../views/Drive.vue'),
  },
  {
    path: '/entity/:entityName',
    name: 'Entity',
    component: () => import('../views/Entity.vue'),
    children: [
      {
        path: 'management',
        name: 'Management',
        component: () => import('../views/management/Management.vue'),
        children: [
          {
            path: 'entities',
            name: 'Entities',
            component: () => import('../views/management/Entities.vue'),
          },
          {
            path: 'users',
            name: 'Users',
            component: () => import('../views/management/Users.vue'),
          },
          {
            path: 'roles',
            name: 'Roles',
            component: Roles,
          },
        ],
      },
      {
        path: 'supportResource',
        name: 'SupportResource',
        component: () => import('../views/supportResource/SupportResource.vue'),
      },
      {
        path: 'collaborativeWork',
        name: 'CollaborativeWork',
        component: () => import('../views/collaborativeWork/CollaborativeWork.vue'),
      },
      {
        path: 'directMessage',
        name: 'DirectMessage',
        component: () => import('../views/DirectMessage.vue'),
      },
      {
        path: 'tests',
        component: () => import('../views/Tests.vue'),
        children: [
          {
            path: '/',
            name: 'ShowTests',
            component: () => import('../components/ehqModule/MainTests.vue'),
          },
          {
            path: 'questionBank',
            name: 'QuestionBank',
            component: () => import('../components/ehqModule/MainQuestionBank.vue'),
          },
          {
            path: 'areasQuestionBank',
            name: 'AreasQuestionBank',
            component: () => import('../components/ehqModule/MainAreasQB.vue'),
          },
        ],
      },
      {
        path: 'ehq',
        component: () => import('../views/Ehq.vue'),
        children: [
          {
            path: '/',
            name: 'Main',
            component: () => import('../components/ehq/notification/Main.vue'),
          },
          {
            path: 'areas',
            name: 'ManageAreas',
            component: () => import('../components/ehq/areas/Main.vue'),
          },
          {
            path: 'questions',
            name: 'QuestionBank2',
            component: () => import('../components/ehq/questionBank/QuestionBank.vue'),
          },
          {
            path: 'tests',
            name: 'TestsPage',
            component: () => import('../components/ehq/tests/Tests.vue'),
          },
          {
            path: 'studentTests',
            name: 'StudentTests',
            component: () => import('../components/ehq/testsStudent/Tests.vue'),
          },
        ],
      },
    ],
    redirect: '/entity/:entityName/supportResource',
  },
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes,
  // faz o scroll voltar para o topo quando clica no router-link
  scrollBehavior() {
    return window.scrollTo({ top: 0, behavior: 'smooth' })
  },
})

export default router
