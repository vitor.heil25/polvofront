import { Utils } from '../utils/utils'
import http from '../utils/httpQb'

export default {
  async readPublicQuestions() {
    let ret = await http.get(`qb/api/platform/question/read`, {
      headers: await Utils.getHeadersQB(),
    })
    return ret.data
  },

  async readQuestionsByArea(areaId) {
    let ret = await http.get(`qb/api/platform/question/read/area/${areaId}`, {
      headers: await Utils.getHeadersQB(),
    })
    return ret.data
  },

  async readQuestionsByTeacher(document) {
    let ret = await http.get(`qb/api/platform/question/read/own/${document}`, {
      headers: await Utils.getHeadersQB(),
    })
    return ret.data
  },

  async newQuestion(data) {
    let ret = await http.post(
      `qb/api/platform/question/create`,
      {
        title: data.title,
        body: data.body,
        questionType: data.questionType,
        visibility: data.visibility,
        difficulty: data.difficulty,
        correctAnswers: data.correctAnswers,
        incorrectAnswers: data.incorrectAnswers,
        author: data.author,
        area: data.area,
      },
      { headers: await Utils.getHeadersQB() }
    )
    return ret.data
  },

  async getAllAvailableAreas() {
    let ret = await http.get(`qb/api/platform/area/readAll`, {
      headers: await Utils.getHeadersQB(),
    })
    return ret.data
  },

  // para editar outros dados como: titulo, dificuldade, tipo, visibilidade, enunciado
  async updateQuestion(teacherId, questionId, update) {
    let ret = await http.post(
      `qb/api/platform/question/update`,
      {
        teacherId: teacherId,
        questionId: questionId,
        update: update,
      },
      { headers: await Utils.getHeadersQB() }
    )
    return ret.data
  },

  async deleteQuestion(teacherId, questionId) {
    let ret = await http.post(
      `qb/api/platform/question/remove`,
      {
        teacherId: teacherId,
        questionId: questionId,
      },
      { headers: await Utils.getHeadersQB() }
    )
    return ret.data
  },

  // editando-> removendo resposta da pergunta -> answers = []
  async editRemoveAnswer(teacherId, questionId, answers) {
    let ret = await http.post(
      `qb/api/platform/question/remove/answers`,
      {
        teacherId: teacherId,
        questionId: questionId,
        answers: answers,
      },
      { headers: await Utils.getHeadersQB() }
    )
    return ret.data
  },

  //answers é obj que contem correctAnswers e incorrectAnswers
  async editAddAnswer(teacherId, questionId, answers) {
    let ret = await http.post(
      `qb/api/platform/question/add/answers`,
      {
        teacherId: teacherId,
        questionId: questionId,
        answers: answers,
      },
      { headers: await Utils.getHeadersQB() }
    )
    return ret.data
  },

  // QuestionBank gerenciar Areas de Conhecimento
  async readRootAreas() {
    let ret = await http.get(`qb/api/platform/area/read`, {
      headers: await Utils.getHeadersQB(),
    })
    return ret.data
  },

  async readChildrenArea(parentId) {
    let ret = await http.get(`qb/api/platform/area/children/read/${parentId}`, {
      headers: await Utils.getHeadersQB(),
    })
    return ret.data
  },

  async createArea(area) {
    let ret = await http.post(
      'qb/api/platform/area/create',
      {
        name: area.name,
      },
      { headers: await Utils.getHeadersQB() }
    )
    return ret.data
  },
  async createSubArea(subArea) {
    let ret = await http.post(
      'qb/api/platform/area/children/create',
      {
        name: subArea.name,
        daddy: subArea.daddy,
      },
      { headers: await Utils.getHeadersQB() }
    )
    return ret.data
  },
  async moveSubArea(data) {
    let ret = await http.post(
      'qb/api/platform/area/move',
      {
        areaId: data.areaId,
        from: data.from,
        to: data.to,
      },
      { headers: await Utils.getHeadersQB() }
    )
    return ret.data
  },
  async updateArea(areaId, update) {
    let ret = await http.post(
      'qb/api/platform/area/update',
      {
        areaId: areaId,
        update: {
          name: update.name,
        },
      },
      { headers: await Utils.getHeadersQB() }
    )
    return ret.data
  },
  async deleteArea(areaId, parentId) {
    const path = parentId
      ? `qb/api/platform/area/delete/${areaId}/?parentId=${parentId}`
      : `qb/api/platform/area/delete/${areaId}`
    let ret = await http.delete(path, {
      headers: await Utils.getHeadersQB(),
    })
    return ret.data
  },

  // EDITAR PERGUNTAS
  async editRemoveAnswer(teacherId, questionId, answers) {
    let ret = await http.post(
      `qb/api/platform/question/remove/answers`,
      {
        teacherId: teacherId,
        questionId: questionId,
        answers: answers,
      },
      {
        headers: await Utils.getHeadersQB(),
      }
    )
    return ret.data
  },

  async editAddAnswer(teacherId, questionId, answers) {
    let ret = await http.post(
      `qb/api/platform/question/add/answers`,
      {
        teacherId: teacherId,
        questionId: questionId,
        answers: answers,
      },
      {
        headers: await Utils.getHeadersQB(),
      }
    )
    return ret.data
  },

  async editRemoveArea(teacherId, questionId, areas) {
    let ret = await http.post(
      `qb/api/platform/question/remove/area`,
      {
        teacherId: teacherId,
        questionId: questionId,
        areas: areas,
      },
      {
        headers: await Utils.getHeadersQB(),
      }
    )
    return ret.data
  },

  async editAddArea(teacherId, questionId, areas) {
    let ret = await http.post(
      `qb/api/platform/question/add/area`,
      {
        teacherId: teacherId,
        questionId: questionId,
        areas: areas,
      },
      {
        headers: await Utils.getHeadersQB(),
      }
    )
    return ret.data
  },
}
