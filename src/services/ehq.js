import http from './http'
import { Utils } from '../utils/utils'

// EHQ - módulo do Polvo
export default {
  /* mostra todos os testes disponíveis */
  async readAllTests(entityId, actionId) {
    let ret = await http.post(
      `/api/ehq/${entityId}/${actionId}/readAllTest`,
      {},
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },

  /* cria um teste */
  async createTest(entityId, actionId, data) {
    let ret = await http.post(`/api/ehq/${entityId}/${actionId}/testCreate`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },

  /* atualiza um teste */
  async updateTest(entityId, actionId, id, initialDate, endDate, title, instructions) {
    let ret = await http.post(
      `/api/ehq/${entityId}/${actionId}/testUpdate`,
      {
        id: id,
        update: {
          initialDate: initialDate,
          endDate: endDate,
          title: title,
          instructions: instructions,
        },
      },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  /* deleta um teste */
  async deleteTest(entityId, actionId, id) {
    let ret = await http.post(
      `/api/ehq/${entityId}/${actionId}/testDelete`,
      {
        id: id,
      },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  /* permite ao professor ver todos testes que foram submetidos pelos alunos */
  async readAllAppliedTests(entityId, actionId, testId) {
    let ret = await http.post(
      `/api/ehq/${entityId}/${actionId}/teacherTestApplicationRead`,
      { testId: testId },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },

  /* Métodos p/ monitor */

  async monitorInitTest(entityId, actionId, monitorId) {
    let ret = await http.post(
      `/api/ehq/${entityId}/${actionId}/monitorTestEnter`,
      { monitorId: monitorId },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },

  async monitorReadInfoTest(entityId, actionId, monitorId) {
    let ret = await http.post(
      `/api/ehq/${entityId}/${actionId}/monitorReadTest`,
      { monitorId: monitorId },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },

  /* Métodos p/ aluno */
  async studentStep1(entityId, actionId, studentId) {
    let ret = await http.post(
      `/api/ehq/${entityId}/${actionId}/studentTestEnter`,
      { entityId: entityId, studentId: studentId },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },

  async studentStep2(entityId, actionId, studentId, testId) {
    let ret = await http.post(
      `/api/ehq/${entityId}/${actionId}/studentGetTest`,
      { studentId: studentId, testId: testId },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },

  // data: { studentId,testId,questionId,answers: [_id,order,text] }
  async studentAnswerQuestion(entityId, actionId, data) {
    let ret = await http.post(
      `/api/ehq/${entityId}/${actionId}/studentQuestionAnswer`,
      {
        studentId: data.studentId,
        testId: data.testId,
        questionId: data.questionId,
        answers: data.answers,
      },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
}
