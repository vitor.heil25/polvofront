import http from './http'
import { Utils } from '../utils/utils'

export default {
  //parâmetro msg: { sendBy,[sendTo],title,subject,sendDate,hasAttachment }
  async createMessage(entityId, actionId, msg) {
    let ret = await http.post(`/api/directMessage/${entityId}/${actionId}/createMessage`, msg, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  //adiciona arquivo anexo a msg
  async attachFile(entityId, actionId, idMessage, fileName, formData) {
    let ret = await http.post(
      `/api/directMessage/${entityId}/${actionId}/attachDocument/${idMessage}/${fileName}`,
      formData,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  //mostra todas mensagens recebidas **não está funcionando corretamente**
  async readAllMessages(entityId, actionId) {
    let ret = await http.post(
      `/api/directMessage/${entityId}/${actionId}/readAllUserMails`,
      {},
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  //mostra todas mensagens recebidas e não marcadas como lidas
  async readAllNewMessages(entityId, actionId) {
    let ret = await http.post(
      `/api/directMessage/${entityId}/${actionId}/readUserAvailableMails`,
      {},
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },

  /*quando o usuario clica em uma msg não lida, ele marca como lida e não aparece mais nas não lidas.
  msgid -> {mailId: "id"}*/

  async updateReadMessage(entityId, actionId, msgId) {
    let ret = await http.post(
      `/api/directMessage/${entityId}/${actionId}/updateUserReadMails`,
      msgId,
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },

  //mostra todas mensagens enviadas pelo usuario
  async readUserSentMessage(entityId, actionId) {
    let ret = await http.post(
      `/api/directMessage/${entityId}/${actionId}/readUserSentMails`,
      {},
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
}
