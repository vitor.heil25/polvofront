import http from './http'
import { Utils } from '../utils/utils'

export default {
  async createFolder(entityId, actionId, data) {
    let ret = await http.post(`/api/supportResource/${entityId}/${actionId}/createFolder`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async readSupportResource(entityId, actionId) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/readSupportResource`,
      null,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  async addLocalFile(entityId, actionId, fileName, name, formData) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/addLocalFile/${fileName}/${name}`,
      formData,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  //INIT: adicionar dentro de pasta

  // async addFileOnFolder(entityId, actionId, fileName, supportResourceId, formData) {
  //   let ret = await http.post(
  //     `/api/supportResource/${entityId}/${actionId}/addFileOnFolder/${fileName}/${supportResourceId}`,
  //     formData,
  //     {
  //       headers: await Utils.getHeaders(),
  //     }
  //   )
  //   return ret.data
  // },

  //aqui
  async addFilesOnFolder(entityId, actionId, fileName,parentId,formData) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/addFilesOnFolder/${fileName}/${parentId}`,
      formData,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },


  
  //data: {parentId, folder:{name:"nome da pasta"}}
  async addFolderOnFolder(entityId, actionId, data) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/createChildrenFolder/`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  //data: {parentId, externalLink:{name:"nome do link", link: "url"}}
  async addExternalLinkOnFolder(entityId, actionId, data) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/createChildrenLink/`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  //END: adicionar dentro de pasta

  async addExternalLink(entityId, actionId, data) {
    let ret = await http.post(`/api/supportResource/${entityId}/${actionId}/createLink`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },

  async downloadSupportResource(entityId, actionId, data, fileName) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/downloadSupportResource`,
      data,
      { headers: await Utils.getHeaders(), responseType: 'blob' }
    )
    const url = window.URL.createObjectURL(new Blob([ret.data]))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', fileName)
    document.body.appendChild(link)
    link.click()
    return ret.data
  },
  async deleteSupportResource(entityId, actionId, data) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/deleteSupportResource`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async deleteSupportResource(entityId, actionId, data) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/deleteSupportResource`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  async updateSupportResource(entityId, actionId, data) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/updateSupportResource`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  async updateFileName(entityId, actionId, data){
    console.log(data)
    console.log("entityId: ", entityId,
        "actionId: ", actionId,
        "data: ", data )

    let ret = await http.put(
      `/api/supportResource/${entityId}/${actionId}/updateFileName`,
      data,
      {
        headers: await Utils.getHeaders()
      }
    )
    return ret
  },

  // INIT importar do drive
  async linkFileDrive(entityId, actionId, data) {
    let ret = await http.post(`/api/supportResource/${entityId}/${actionId}/linkFileDrive`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async linkFolderDrive(entityId, actionId, data) {
    let ret = await http.post(
      `/api/supportResource/${entityId}/${actionId}/linkFolderDrive`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  //data = {linkId: "", name: ""}
  async linkExternalLinkDrive(entityId, actionId, data) {
    let ret = await http.post(`/api/supportResource/${entityId}/${actionId}/linkFromDrive`, 
    data,
    {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  // END importar do drive

  async readFolderInfo(supportResourceId, parentId) {
    const path = parentId
      ? `/api/supportResource/${supportResourceId}/folder?folderId=${parentId}`
      : `/api/supportResource/${supportResourceId}/folder`

    let ret = await http.get(path, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
}
