import axios from 'axios'

/**
 * Cria a instância do axios, podendo setar diversas configs, como headers, endpoint, etc..
 */

const client = axios.create({
  baseURL: 'http://localhost:5000/', // Local
  // baseURL: 'http://179.97.96.69:7000/ehq/', // UDESC
  // baseURL: 'http://178.128.69.140:80/ehq/', // digital OCEAN
})

export default client