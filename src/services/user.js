import http from './http'
import { Utils } from '../utils/utils'

export default {
  async logout() {
    let ret = await http.post('/api/user/logout', {}, { headers: await Utils.getHeaders() })
    return ret
  },
  async updatePassword(update) {
    let ret = await http.post('/api/user/password', update, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async readUserEntities() {
    let ret = await http.get('/api/user/entities', {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async readEntityChild(entityId) {
    let ret = await http.get(`/api/user/entity/children/${entityId}`, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
}
