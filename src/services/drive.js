import http from './http'
import { Utils } from '../utils/utils'

export default {
  async readFoldercontent(parentId) {
    const path = parentId ? `/api/drive/folder?parentId=${parentId}` : `/api/drive/folder`,
      ret = await http.get(path, {
        headers: await Utils.getHeaders(),
      })
    return ret.data
  },
  async createFolder(name, parentId) {
        const ret = await http.post(
      '/api/drive/folder',
      { name, parentId },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async renameFolder(newName, folderId) {
    const ret = await http.put(
      '/api/drive/folder/rename',
      { newName, folderId },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async renameFile(newName, fileId) {
    const ret = await http.put(
      '/api/drive/file/rename',
      { newName, fileId },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async deleteFile(fileId, parentId) {
    const path = parentId
      ? `/api/drive/file/${fileId}/?parentId=${parentId}`
      : `/api/drive/file/${fileId}`
    const ret = await http.delete(path, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },

  async deleteFolder(folderId, parentId) {
    const path = parentId
      ? `/api/drive/folder/${folderId}/?parentId=${parentId}`
      : `/api/drive/folder/${folderId}`
    const ret = await http.delete(path, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },

  async uploadFile(fileName, formData, parentId) {
    let path = `/api/drive/file/${fileName}`
    if (parentId) path = `${path}?parentId=${parentId}`
    const ret = await http.post(path, formData, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async downloadFile(fileId, fileName, fileExtension) {
    const path = `/api/drive/file/${fileId}`
    const ret = await http.get(path, {
      headers: await Utils.getHeaders(),
      responseType: 'blob',
    })
    const url = window.URL.createObjectURL(new Blob([ret.data]))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', fileName + fileExtension)
    document.body.appendChild(link)
    link.click()
  },
  //external link
  async createExternalLink(name, link, parentId) {
    const ret = await http.post(
      '/api/drive/link',
      { name, link, parentId },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  //update = {name:"" , link:""}
  async renameExternalLink(linkId, update) {
    const ret = await http.put(
      '/api/drive/link',
      { linkId, update },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  async deleteLink(linkId, parentId) {
    const path = parentId
      ? `/api/drive/link/${linkId}/?parentId=${parentId}`
      : `/api/drive/link/${linkId}`
    const ret = await http.delete(path, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
}
