import http from './http'
import { Utils } from '../utils/utils'

export default {
  async createPrivilege(entityId, actionId, role) {
    let ret = await http.post(`/api/management/${entityId}/${actionId}/privilegeCreate`, role, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async readPrivilege(entityId, actionId) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/privilegesRead`,
      {},
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async updatePrivilege(entityId, actionId, id, update) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/privilegeUpdate`,
      { id, update },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async removePrivilege(entityId, actionId, id) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/privilegeDelete`,
      { id },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async readEntityModules(entityId, actionId) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/moduleRead`,
      { entityId },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async createEntity(entityId, actionId, entity) {
    let ret = await http.post(`/api/management/${entityId}/${actionId}/entityCreate`, entity, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async readEntitiesByPage(entityId, actionId, search) {
    let ret = await http.post(`/api/management/${entityId}/${actionId}/entityReadPerPage`, search, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  // entityId espera um objeto como esse -> {id: "i203192839021903"}
  async readEntityPerId(entityId, actionId, id) {
    let ret = await http.post(`/api/management/${entityId}/${actionId}/entityReadPerId`, id, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async updateEntity(entityId, actionId, id, update) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/entityUpdate`,
      { id, update },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async removeEntity(entityId, actionId, removedEntityId) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/entityDelete`,
      { id: removedEntityId },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async readEntityByName(entityId, actionId, name) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/entityReadByName`,
      { name },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async addEntityOn(entityId, actionId, data) {
    let ret = await http.post(`/api/management/${entityId}/${actionId}/entityAddOn`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async removeEntityFrom(entityId, actionId, entities) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/removeEntityFrom`,
      entities,
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async downloadEntities(entityId, actionId) {
    let ret = await http.post(
      `/api/download/${entityId}/${actionId}/entitiesXLSX`,
      {},
      { headers: await Utils.getHeaders(), responseType: 'blob' }
    )
    const url = window.URL.createObjectURL(new Blob([ret.data]))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', 'a.xlsx')
    document.body.appendChild(link)
    link.click()
    return ret.data
  },
  async importEntities(entityId, actionId, field) {
    let ret = await http.post(`/api/upload/${entityId}/${actionId}/entitiesImport`, field, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },

  async createUser(entityId, actionId, user) {
    let ret = await http.post(`/api/management/${entityId}/${actionId}/userCreate`, user, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async readUserEntities(entityId, actionId, id) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/userReadEntities`,
      { id },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async updateUser(entityId, actionId, update, id) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/userUpdate`,
      { id, update },
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async removeUser(entityId, actionId, removedUserId) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/userDelete`,
      { usersId: removedUserId },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async importUsers(entityId, actionId, privilegesId, field) {
    let ret = await http.post(
      `/api/upload/${entityId}/${actionId}/usersImport/${privilegesId}`,
      field,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async readUsersByPage(entityId, actionId, search) {
    let ret = await http.post(`/api/management/${entityId}/${actionId}/userReadPerPage`, search, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async readUsersByFilter(entityId, actionId, filterType, data) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/userReadByFilter`,
      { filterType, data },
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async addUserOnEntity(entityId, actionId, data) {
    let ret = await http.post(`/api/management/${entityId}/${actionId}/userAddOnEntity`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async removeUserFromEntity(entityId, actionId, data) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/userRemoveFromEntity`,
      data,
      { headers: await Utils.getHeaders() }
    )
    return ret.data
  },
  async changeUserPrivileges(entityId, actionId, data) {
    let ret = await http.post(`/api/management/${entityId}/${actionId}/userChangePrivilege`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async readAllUserBD(entityId, actionId) {
    let ret = await http.post(
      `/api/management/${entityId}/${actionId}/readAllUser`,
      {},
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
}
