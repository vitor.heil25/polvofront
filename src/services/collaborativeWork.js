import http from './http'
import { Utils } from '../utils/utils'

export default {
  async readAll(entityId, actionId) {
    let ret = await http.post(`/api/collaborativeWork/${entityId}/${actionId}/readAll`, null, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },

  async readAllAvailable(entityId, actionId) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/readAllAvailable`,
      null,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  async createCollaborativeWork(entityId, actionId, data) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/createCollaborativeWork`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },

  // data: collaborativeWorkId, answer: {title, comment})
  async createAnswersCollaborativeWork(entityId, actionId, data) {
    let ret = await http.post(`/api/collaborativeWork/${entityId}/${actionId}/createAnswer`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async readCollaborativeWorkSentAnswers(entityId, actionId, data) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/readCollaborativeWorkSentAnswers`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  /*
  mostra apenas as respostas da qual o usuário faz parte do time
  data: {collaborativeWorkAnswerId}
  */
  async readAvailableAnswers(entityId, actionId, data) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/readCollaborativeWorkAvailableAnswers`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async readCollaborativeWorkAnswersDetails(entityId, actionId, data) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/readAnswerDetail`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  /* permite ao usuario responder uma resposta (para professor)
  reply: {collaborativeWorkAnswerId: string, teamId: string, answerId: string, answer: {title: "", comment: ""}, 
  userLabel: "Professor" // nao é obrigatorio
  */
  async replyAnswer(entityId, actionId, reply) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/updateAnswerData`,
      reply,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  /* permite ao usuario deletar uma resposta do seu time
  replyToDelete: {id: trabalho colaborativo, teamId: string, answerId: string, answer: string}
  */
  async deleteAnswer(entityId, actionId, replyToDelete) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/deleteAnswer`,
      replyToDelete,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  /* permite ao usuario atualizar informações sobre retroções para respostas
  update: {id: 'String, update: {maxPerTeam: Number, initialDate: Number, endDate: Number} }

  id = id do trabalho colaborativo
  */
  async updateAnswerInfos(entityId, actionId, update) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/updateAnswerInfos`,
      update,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  /* permite adicionar outro integrante ao grupo
  update: {collaborativeWorkAnswerId: id da resposta tc, teamId: id do time, participantId: idUsuario a ser adicionado} }
  */
  async updateParticipants(entityId, actionId, update) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/updateParticipants`,
      update,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  /* permite adicionar outro integrante ao grupo
  update: {answerId: id da resposta tc, teamId: id do time, participantId: idUsuario a ser deletado} }
  */
  async deleteParticipant(entityId, actionId, update) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/deleteParticipant`,
      update,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async deleteCollaborativeWork(entityId, actionId, data) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/deleteCollaborativeWork`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async updateInfos(entityId, actionId, data) {
    let ret = await http.post(`/api/collaborativeWork/${entityId}/${actionId}/updateInfos`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async removeFile(entityId, actionId, data) {
    let ret = await http.post(`/api/collaborativeWork/${entityId}/${actionId}/removeFile`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async addExternalLink(entityId, actionId, data) {
    // let ret = await http.post(`/api/collaborativeWork/${entityId}/${actionId}/externalLink`, data, {
    //   headers: await Utils.getHeaders(),
    // })
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/createLinkCollaborativeWork`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  // data: {id: trablaho colaborativo, linkId: id do link, update:{name:"",link:"link"}}
  async updateExternalLink(entityId, actionId, data) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/updateExternalLink`,
      data,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  // data: {id: trablaho colaborativo, linkId: id do link}
  async deleteExternalLink(entityId, actionId, data) {
    let ret = await http.post(`/api/collaborativeWork/${entityId}/${actionId}/removeLink`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async addLocalFile(entityId, actionId, fileName, collaborativeWorkId, formData) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/addLocalFile/${fileName}/${collaborativeWorkId}`,
      formData,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  async downloadFile(entityId, actionId, data, fileName) {
    let ret = await http.post(`/api/collaborativeWork/${entityId}/${actionId}/downloadFile`, data, {
      headers: await Utils.getHeaders(),
      responseType: 'blob',
    })
    const url = window.URL.createObjectURL(new Blob([ret.data]))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', fileName)
    document.body.appendChild(link)
    link.click()
    return ret.data
  },
  /* 
  data = {collaborativeWorkAnswerID: '', teamId: '', answerId: '', fileId: ''}
  */
  async downloadFileAnswer(entityId, actionId, data, fileName) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/downloadFileAnswer`,
      data,
      {
        headers: await Utils.getHeaders(),
        responseType: 'blob',
      }
    )
    const url = window.URL.createObjectURL(new Blob([ret.data]))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', fileName)
    document.body.appendChild(link)
    link.click()
    return ret.data
  },

  async downloadZipAnswers(entityId, actionId, data, fileName) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/downloadZipAnswers`,
      data,
      {
        headers: await Utils.getHeaders(),
        responseType: 'blob',
      }
    )
    const url = window.URL.createObjectURL(new Blob([ret.data], { type: 'application/zip' }))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', fileName)
    document.body.appendChild(link)
    link.click()
    return ret
  },
  async attachAnswerFile(
    entityId,
    actionId,
    collaborativeWorkAnswerId,
    teamId,
    teamAnswerId,
    fileName,
    formData
  ) {
    let ret = await http.post(
      `/api/collaborativeWork/${entityId}/${actionId}/attachAnswerFile/${collaborativeWorkAnswerId}/${teamId}/${teamAnswerId}/${fileName}`,
      formData,
      {
        headers: await Utils.getHeaders(),
      }
    )
    return ret.data
  },
  // data: {collaborativeWorkAnswerId: id de resposta do trabalho colaborativo}
  async createTeam(entityId, actionId, data) {
    let ret = await http.post(`/api/collaborativeWork/${entityId}/${actionId}/createTeam`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  },
  async deleteTeam(entityId, actionId, data){
    let ret = await http.post(`/api/collaborativeWork/${entityId}/${actionId}/deleteTeam`, data, {
      headers: await Utils.getHeaders(),
    })
    return ret.data
  }
}
