import http from './http'

export default {
  async logIn(user) {
    let ret = await http.post('/api/login/hoken', {
      login: user.email,
      password: user.password,
    })
    return ret.data
  },
}
