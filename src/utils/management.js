import localforage from 'localforage'
import ManagementS from '../services/management'

export class Management {
  static async readModules() {
    const actionName = 'readEntityByName'
    let actionId = await this.getActionIdByName(actionName)
    const routedEntity = await localforage.getItem('routedEntity')
    const entityId = routedEntity.id
    try {
      let ret = await ManagementS.readEntityModules(entityId, actionId)
      return ret
    } catch (e) {
      console.log('e', e)
    }
  }

  static async getActionIdByName(actionName) {
    const routedEntity = await localforage.getItem('routedEntity')
    console.log(routedEntity)
    const userPrivilegesActions = routedEntity.privileges.actions
    const privilege = userPrivilegesActions.find((el) => el.name === actionName)
    if (privilege) {
      return privilege.id
    } else {
      return { success: 'false', message: 'cant find an privilege with that action name' }
    }
  }
}
