function validation($event, validationType) {
  //aceita apenas letras
  if (validationType == 'onlyLetters') {
    let keyCode = $event.keyCode ? $event.keyCode : $event.which
    // only allow letters
    if (
      (keyCode > 64 && keyCode < 91) ||
      (keyCode > 96 && keyCode < 123) ||
      (keyCode > 191 && keyCode <= 255) ||
      keyCode == 32
    ) {
      return
    } else {
      $event.preventDefault()
    }
  }
  //aceita apenas numeros
  if (validationType == 'onlyNumber') {
    let keyCode = $event.keyCode ? $event.keyCode : $event.which
    // only allow numbers
    if (keyCode < 48 || keyCode > 57) {
      $event.preventDefault()
    }
  }
}

export default { validation }
