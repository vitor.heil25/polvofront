import localforage from 'localforage'

export class Utils {
  /**
   * Busca através do sessionStorage os headears para fazer uma requisição com axios.
   */
  static async getHeaders() {
    let userInfo = await localforage.getItem('userInfo')
    const accessKey = await localforage.getItem('accessKey')
    const authenticationKey = userInfo.authenticationKey
    return {
      'access-key': accessKey || (await sessionStorage.getItem('accessKey')),
      'authentication-key':
        authenticationKey || (await sessionStorage.getItem('authenticationKey')),
    }
  }

  static async getHeadersQB() {
    let userInfo = await localforage.getItem('userInfo')
    const accessKey = await localforage.getItem('accessKey')
    const authenticationKey = userInfo.authenticationKey
    const questionBankKey = '5cdb0f752539da05943ba4cb'
    return {
      'platform-key': questionBankKey,
    }
  }

  // Todo: make it recursive to search inside objects aswell
  /*  static clearProp(prop) {
    for (let attr in prop) {
      if (prop.hasOwnProperty(attr)) prop[attr] = ''
    }
  } */

  // Accepts both miliseconds or seconds timestamp, converts it to a 'XX/XX/XXXX' Date
  static timeConverter(timestamp) {
    let date = new Date(timestamp)
    const day = date.getDate() > 9 ? date.getDate() : '0' + date.getDate()
    // console.log(date.getMonth())
    const month = date.getMonth() + 1 >= 10 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)
    const hour = date.getHours() > 9 ? date.getHours() : `0${date.getHours()}`
    const min = date.getMinutes() > 9 ? date.getMinutes() : `0${date.getMinutes()}`
    return `${day}/${month}/${date.getFullYear()}`
  }

  static timeConverter2(stringDate) {
    //formato 10/10/2020
    let day = Number(stringDate.slice(0, 2))
    let month = Number(stringDate.slice(3, 5) - 1)
    let year = Number(stringDate.slice(6, 10))
    let date = new Date(year, month, day)
    let timestamp = date.getTime()
    return timestamp
  }

  static compareDate(timestamp, html) {
    //html -> boolean: true or false

    const actualDate = new Date()
    const actualDay = actualDate.getDate()
    const actualMonth = actualDate.getMonth()
    const actualYear = actualDate.getFullYear()

    let dataItem = new Date(timestamp)
    let dataDay = dataItem.getDate()
    let dataMonth = dataItem.getMonth()
    let dataYear = dataItem.getFullYear()
    let hour = dataItem.getHours() > 9 ? dataItem.getHours() : `0${dataItem.getHours()}`
    let min = dataItem.getMinutes() > 9 ? dataItem.getMinutes() : `0${dataItem.getMinutes()}`

    if (actualDay == dataDay && actualMonth == dataMonth && actualYear == dataYear) {
      if (html == 'noHtml') return `hoje às ${hour}:${min}h`
      return `<b>Hoje</b> às ${hour}:${min}h`
    } else {
      dataDay = dataItem.getDate() > 9 ? dataItem.getDate() : '0' + dataItem.getDate()
      dataMonth =
        dataItem.getMonth() + 1 >= 10 ? dataItem.getMonth() + 1 : '0' + (dataItem.getMonth() + 1)

      return `${dataDay}/${dataMonth}/${dataYear} às ${hour}:${min}h`
    }
  }
}
