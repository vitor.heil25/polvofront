export default {
  /*
  User and Process Configuration
   */
  updateHimselfRole: false,
  alert: { status: false, message: 'Mensagem', color: 'green', icon: 'mdi-check-circle' },
  idActualEntity: '',
  loading: false,
  isLogged: false,
  userInfo: {},
  rootEntities: {},
  routedEntity: {},
  routedEntityChildren: {},
  routedEntityRoles: [],
  toolbarEntities: [],
  entityModules: [], // mostra os modulos da entidade atual
  mappedActions: [], // mostra as ações permitidas ao usuário na entidade atual
  methods: new Set(),

  // Dialogs

  changePassword: false,
  baseSnackbar: {
    open: false,
    text: '',
  },

  //DirectMessage
  resetMailTo: false,

  /// Management
  refreshNameEntities: false,

  // Roles

  rolesCreate: false,
  rolesUpdate: {
    open: false,
    roleUpdated: {
      label: '',
      id: '',
      actions: [],
    },
  },
  rolesDelete: {
    open: false,
    roleRemoved: {
      label: '',
      id: '',
    },
  },

  // Entities

  entitiesCreate: false,
  entitiesUpdate: {
    open: false,
    id: '',
    entity: {
      id: '',
      parentsId: [],
      name: '',
      modules: [],
      firstName: '',
      visible: false,
      initialDate: '',
      endDate: '',
    },
  },
  entitiesDelete: {
    open: false,
    entityDeleted: {
      id: '',
      name: '',
    },
  },
  entitiesManage: {
    open: false,
    entityManaged: {
      id: '',
      name: '',
    },
  },

  // Users

  usersCreate: false,
  usersUpdate: {
    open: false,
    user: {
      id: '',
      name: '',
      surname: '',
      birthday: '',
      matriculation: '',
      email: '',
      document: {
        documentType: '',
        documentNumber: '',
      },
    },
  },
  usersDelete: {
    open: false,
    user: {
      name: '',
      id: '',
    },
  },
  usersManage: {
    open: false,
    entities: {},
    id: '',
  },
  //gambiarras
  cleanAdvancedCollaborativeWork: 0,
  isAdmin: false,
  modulesMaxManagements: {
    createPrivilege: '',
    readPrivilege: '',
    updatePrivilege: '',
    createEntity: '',
    readEntityByName: '',
    updateEntity: '',
    addEntityOn: '',
    removeEntityFrom: '',
    entitiesImport: '',
    entitiesDownload: '',
    userCreate: '',
    userRead: '',
    userUpdate: '',
    userAddOnEntity: '',
    userRemoveFromEntity: '',
    userChangePrivilege: '',
    usersImport: '',
    userRemove: '',
    entityDelete: '',
    privilegeDelete: '',
    readEntityChildren: '',
  },
  //fechando dialogs com esc
  closeDialog: false,
}
