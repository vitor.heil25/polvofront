import localforage from 'localforage'

const DO_LOGIN = (state, payload) => {
  state.userInfo = payload
}

const UPDATE_ALERT = (state, payload) => {
  state.alert = Object.assign(state.alert, payload)
}

const UPDATE_LOADING = (state, payload) => {
  state.loading = payload
}

const UPDATE_LOGGED = (state, payload) => {
  state.isLogged = payload
}

const UPDATE_IDACTUALENTITY = (state, payload) => {
  state.idActualEntity = payload
}

const UPDATE_USER_CHANGE_PASSWORD = (state, payload) => {
  state.changePassword = payload
}

const UPDATE_BASE_SNACKBAR = (state, payload) => {
  state.baseSnackbar = payload
}

const UPDATE_ROOT_ENTITIES = (state, payload) => {
  state.rootEntities = payload
}

const UPDATE_ROUTED_ENTITY = async (state, payload) => {
  const routed = state.rootEntities.find((el) => el.id === payload)
  await localforage.setItem('routedEntity', routed)
  state.routedEntity = routed
}

const UPDATE_ENTITY_CHILDREN = (state, payload) => {
  state.routedEntityChildren = payload
}

const UPDATE_ROUTED_ENTITY_ROLES = (state, payload) => {
  state.routedEntityRoles = payload
}

const UPDATE_TOOLBAR_ENTITIES = (state, payload) => {
  state.toolbarEntities = payload
}

const UPDATE_MAPPED_ACTIONS = (state, payload) => {
  state.mappedActions = payload
}

const UPDATE_SET_ACTIONS = (state, payload) => {
  state.methods = payload
}

// MANAGEMENT

const UPDATE_ENTITY_MODULES = (state, payload) => {
  state.entityModules = payload
}

// ROLES

const UPDATE_ROLES_CREATE = (state, payload) => {
  state.rolesCreate = payload
}
const UPDATE_ROLES_UPDATE = (state, payload) => {
  state.rolesUpdate = payload
}
const UPDATE_ROLES_DELETE = (state, payload) => {
  state.rolesDelete = payload
}

// Entities

const UPDATE_ENTITIES_CREATE = (state, payload) => {
  state.entitiesCreate = payload
}

const UPDATE_ENTITIES_UPDATE = (state, payload) => {
  state.entitiesUpdate = payload
}

const UPDATE_ENTITIES_DELETE = (state, payload) => {
  state.entitiesDelete = payload
}
const UPDATE_ENTITIES_MANAGE = (state, payload) => {
  state.entitiesManage = payload
}

// Users

const UPDATE_USERS_CREATE = (state, payload) => {
  state.usersCreate = payload
}
const UPDATE_USERS_UPDATE = (state, payload) => {
  state.usersUpdate = payload
}
const UPDATE_USERS_DELETE = (state, payload) => {
  state.usersDelete = payload
}
const UPDATE_USERS_MANAGE = (state, payload) => {
  state.usersManage = payload
}

//DirectMessage
const UPDATE_RESET_MAIL_TO = (state, payload) => {
  state.resetMailTo = payload
}

//Atualiza nome das entidades ao alterar na tela e no menulateral
const UPDATE_REFRESH_NAME_ENTITIES = (state, payload) => {
  state.refreshNameEntities = payload
}
//Atualiza os proprios dados na entidade (muda menu e o texto do papel)
const UPDATE_HIMSELF_ROLE = (state, payload) => {
  state.updateHimselfRole = payload
}

//Check is Admin com todas funcionalidades na entidade
const UPDATE_IS_ADMIN = (state, payload) => {
  state.isAdmin = payload
}

//Limpa os dados do modo avançado do Trabalho colaborativo
const UPDATE_CLEAN_ADVANCED_COLLABORATIVEWORK = (state, payload) => {
  state.cleanAdvancedCollaborativeWork = payload
}

//Fecha Dialog ao apertar ESC
const UPDATE_CLOSE_DIALOG = (state, payload) => {
  state.closeDialog = payload
}

export {
  DO_LOGIN,
  UPDATE_LOADING,
  UPDATE_LOGGED,
  UPDATE_ALERT,
  UPDATE_USER_CHANGE_PASSWORD,
  UPDATE_BASE_SNACKBAR,
  UPDATE_ROOT_ENTITIES,
  UPDATE_ROUTED_ENTITY,
  UPDATE_ENTITY_CHILDREN,
  UPDATE_ROUTED_ENTITY_ROLES,
  UPDATE_TOOLBAR_ENTITIES,
  UPDATE_ROLES_CREATE,
  UPDATE_ROLES_UPDATE,
  UPDATE_ROLES_DELETE,
  UPDATE_ENTITY_MODULES,
  UPDATE_MAPPED_ACTIONS,
  UPDATE_ENTITIES_CREATE,
  UPDATE_ENTITIES_UPDATE,
  UPDATE_ENTITIES_DELETE,
  UPDATE_ENTITIES_MANAGE,
  UPDATE_SET_ACTIONS,
  UPDATE_USERS_CREATE,
  UPDATE_USERS_UPDATE,
  UPDATE_USERS_DELETE,
  UPDATE_USERS_MANAGE,
  UPDATE_RESET_MAIL_TO,
  UPDATE_IS_ADMIN,
  UPDATE_REFRESH_NAME_ENTITIES,
  UPDATE_IDACTUALENTITY,
  UPDATE_HIMSELF_ROLE,
  UPDATE_CLEAN_ADVANCED_COLLABORATIVEWORK,
  UPDATE_CLOSE_DIALOG,
}
