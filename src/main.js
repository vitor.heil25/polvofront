import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './modules'
import vuetify from './plugins/vuetify'
import '@babel/polyfill'
import VueLocalForage from 'vue-localforage'
import AlertConfirm from '@/components/AlertConfirm.vue'

Vue.config.productionTip = false

Vue.use(VueLocalForage)

//componentes globais
Vue.component('AlertConfirm', AlertConfirm)

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app')
