import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import pt from '../i18n/vuetify/pt.js'
import en from '../i18n/vuetify/en.js'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#2a0845',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
        light: '#411A60',
        clear: '#5C347D',
        shadow: '#19022C',
        darkness: '#0D0116',
        drive: '#411A60',
        navDrive: '#e8e8e8',
        test: '#ffcc00',
        white: '#fff',
      },
    },
  },
  lang: {
    locales: { en, pt },
    current: 'pt',
  },
})
