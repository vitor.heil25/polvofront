module.exports = {
  ignore: [/\/core-js/],
  sourceType: 'unambiguous',
  presets: ['@vue/cli-plugin-babel/preset'],
}
